package com.wang.resume;

import cc.sdkutil.controller.core.CCSDKUtil;
import cc.sdkutil.view.CCBaseApplication;

/**
 * Created by wangcong on 15-5-5.
 */
public class ResumeApplication extends CCBaseApplication {

    @Override
    public void onCreate() {
        super.onCreate();

        CCSDKUtil.initialize(this);
    }
}
