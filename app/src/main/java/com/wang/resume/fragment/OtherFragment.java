package com.wang.resume.fragment;

import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import com.wang.resume.R;
import java.util.Map;
import cc.sdkutil.controller.inject.CCInjectUtil;
import cc.sdkutil.model.inject.CCInjectRes;
import cc.sdkutil.view.CCBaseFragment;

/**
 * Created by wangcong on 15-5-7.
 */
public class OtherFragment extends CCBaseFragment {

    public final static String COLOR_BG = "color_bg";
    public final static String POSITION = "position";
    public final static String COLOR_ELEMENT = "color_element";
    public final static String TITLE = "title";
    public final static String FUNC = "func";
    public final static String SKILL = "skill";

    @CCInjectRes(R.id.other_scroll)
    ScrollView scrollView;
    @CCInjectRes(R.id.other_count_indicator)
    TextView txtCount;
    @CCInjectRes(R.id.other_title)
    TextView txtTitle;
    @CCInjectRes(R.id.other_line)
    ImageView imgvLine;
    @CCInjectRes(R.id.other_function_title)
    TextView txtFuncTitle;
    @CCInjectRes(R.id.other_function_content)
    TextView txtFuncContent;
    @CCInjectRes(R.id.other_skill_title)
    TextView txtSkillTitle;
    @CCInjectRes(R.id.other_skill_content)
    TextView txtSkillContent;

    private Map<String, String> mMap;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mMap = (Map<String, String>) getArguments().getSerializable("data");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View currentView = inflater.inflate(R.layout.fragment_other, container, false);
        CCInjectUtil.inject(this, currentView);
        return currentView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        scrollView.setBackgroundResource(Integer.valueOf(mMap.get(COLOR_BG)));
        txtCount.setText(mMap.get(POSITION));
        txtCount.setBackgroundResource(Integer.valueOf(mMap.get(COLOR_ELEMENT)));
        txtTitle.setText(mMap.get(TITLE));
        txtTitle.setTextColor(Integer.valueOf(mMap.get(COLOR_ELEMENT)));
        imgvLine.setBackgroundResource(Integer.valueOf(mMap.get(COLOR_ELEMENT)));
        txtFuncTitle.setTextColor(Integer.valueOf(mMap.get(COLOR_ELEMENT)));
        txtFuncContent.setText(Html.fromHtml(mMap.get(FUNC)));
        txtSkillTitle.setTextColor(Integer.valueOf(mMap.get(COLOR_ELEMENT)));
        txtSkillContent.setText(Html.fromHtml(mMap.get(SKILL)));
    }
}
