package com.wang.resume.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.wang.resume.R;
import com.wang.resume.activity.BasicInfoActivity;
import com.wang.resume.activity.JobActivity;
import com.wang.resume.activity.OtherActivity;
import com.wang.resume.activity.ProjectActivity;
import com.wang.resume.activity.SkillActivity;
import cc.sdkutil.controller.inject.CCInjectUtil;
import cc.sdkutil.model.inject.CCInjectEvent;
import cc.sdkutil.model.inject.CCInjectRes;
import cc.sdkutil.view.CCBaseFragment;

/**
 * Created by wangcong on 15-5-5.
 */
public class MainContentFragment extends CCBaseFragment {

    @CCInjectRes(R.id.content_marquee_text)
    TextView mMarqueeText;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View currentView = inflater.inflate(R.layout.fragment_content, container, false);
        CCInjectUtil.inject(this, currentView);
        return currentView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mMarqueeText.setFocusable(true);
    }

    @CCInjectEvent(value = {R.id.content_call, R.id.content_linear_one, R.id.content_linear_two,
            R.id.content_linear_three, R.id.content_linear_four, R.id.content_linear_five}, clazz = View.OnClickListener.class)
    public void onClick(View v) {
        Intent intent = null;
        switch (v.getId()) {
            case R.id.content_call:
                intent = new Intent();
                intent.setAction(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:15308220151"));
                break;
            case R.id.content_linear_one:
                intent = new Intent(getActivity(), BasicInfoActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                break;
            case R.id.content_linear_two:
                intent = new Intent(getActivity(), SkillActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                break;
            case R.id.content_linear_three:
                intent = new Intent(getActivity(), JobActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                break;
            case R.id.content_linear_four:
                intent = new Intent(getActivity(), ProjectActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                break;
            case R.id.content_linear_five:
                intent = new Intent(getActivity(), OtherActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                break;
            default:
                break;
        }
        startActivity(intent);
    }
}
