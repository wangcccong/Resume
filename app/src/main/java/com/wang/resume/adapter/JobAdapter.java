package com.wang.resume.adapter;

import android.content.Context;
import android.support.v4.util.ArrayMap;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.wang.resume.R;

import java.util.List;
import cc.sdkutil.controller.inject.CCInjectUtil;
import cc.sdkutil.model.inject.CCInjectRes;

/**
 * Created by wangcong on 15-5-5.
 */
public class JobAdapter extends BaseAdapter {

    public final static String TIME_START = "time_start";
    public final static String TIME_END = "time_end";
    public final static String COMPANY_NAME = "company_name";
    public final static String JOB_DETAIL = "job_detail";

    private Context mContext;
    private List<ArrayMap<String, String>> mAllList;

    public JobAdapter(Context context, List<ArrayMap<String, String>> list) {
        this.mContext = context;
        this.mAllList = list;
    }

    @Override
    public int getCount() {
        return mAllList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Object getItem(int position) {
        return mAllList.get(position);
    }

    @Override
    public boolean isEnabled(int position) {
        return false;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.adapter_jobexp, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        ArrayMap<String, String> map = mAllList.get(position);
        holder.update(map);
        if (position == 0) {
            holder.txtTimeStart.setBackgroundColor(mContext.getResources().getColor(R.color.color_five_nor));
            holder.txtTimeEnd.setBackgroundColor(mContext.getResources().getColor(R.color.color_five_pre));
        } else if (position == 1) {
            holder.txtTimeStart.setBackgroundColor(mContext.getResources().getColor(R.color.color_four_nor));
            holder.txtTimeEnd.setBackgroundColor(mContext.getResources().getColor(R.color.color_four_pre));
        } else {
            holder.txtTimeStart.setBackgroundColor(mContext.getResources().getColor(R.color.color_two_nor));
            holder.txtTimeEnd.setBackgroundColor(mContext.getResources().getColor(R.color.color_two_pre));
        }
        return convertView;
    }

    static class ViewHolder {

        @CCInjectRes(R.id.jobadp_time_start)
        TextView txtTimeStart;
        @CCInjectRes(R.id.jobadp_time_end)
        TextView txtTimeEnd;
        @CCInjectRes(R.id.jobadp_company)
        TextView txtCompany;
        @CCInjectRes(R.id.jobadp_job_detail)
        TextView txtJobDetail;

        public ViewHolder(View itemView) {
            CCInjectUtil.inject(this, itemView);
        }

        public void update(ArrayMap<String, String> map) {
            txtTimeStart.setText(map.get(TIME_START));
            txtTimeEnd.setText(map.get(TIME_END));
            txtCompany.setText(map.get(COMPANY_NAME));
            txtJobDetail.setText(Html.fromHtml(map.get(JOB_DETAIL)));
        }
    }
}
