package com.wang.resume.adapter;

import android.content.Context;
import android.support.v4.util.ArrayMap;
import android.support.v7.widget.CardView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.wang.resume.R;
import java.util.List;
import cc.sdkutil.controller.inject.CCInjectUtil;
import cc.sdkutil.model.inject.CCInjectRes;

/**
 * Created by wangcong on 15-5-7.
 */
public class BasicInfoAdapter extends BaseAdapter {

    public final static String TITLE = "title";
    public final static String CONTENT = "content";

    private Context mContext;
    private List<ArrayMap<String, String>> mAllList;

    public BasicInfoAdapter(Context context, List<ArrayMap<String, String>> list) {
        this.mContext = context;
        this.mAllList = list;
    }

    @Override
    public int getCount() {
        return mAllList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Object getItem(int position) {
        return mAllList.get(position);
    }

    @Override
    public boolean isEnabled(int position) {
        return false;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.adapter_basicinfo, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        ArrayMap<String, String> map = mAllList.get(position);
        holder.update(map, position);
        return convertView;
    }

    static class ViewHolder {
        @CCInjectRes(R.id.basic_card)
        CardView cardView;
        @CCInjectRes(R.id.basic_count_indicator)
        TextView txtCount;
        @CCInjectRes(R.id.basic_title)
        TextView txtTitle;
        @CCInjectRes(R.id.basic_line)
        ImageView imgvLine;
        @CCInjectRes(R.id.basic_content)
        TextView txtContent;

        private Context mContext;

        public ViewHolder(View itemView) {
            mContext = itemView.getContext();
            CCInjectUtil.inject(this, itemView);
        }

        public void update(ArrayMap<String, String> map, int position) {
//            AnimationSet set = (AnimationSet) AnimationUtils.loadAnimation(mContext,
//                    position % 2 == 0 ? R.anim.anim_list_item_leftin : R.anim.anim_list_item_rightin);
//            cardView.setAnimation(set);
            txtCount.setText("0" + (position + 1));
            txtTitle.setText(map.get(TITLE));
            txtContent.setText(Html.fromHtml(map.get(CONTENT)));
            if (position % 3 == 0) {
                txtCount.setBackgroundResource(R.color.color_adapter_item_ring_one);
                txtTitle.setTextColor(mContext.getResources().getColor(R.color.color_adapter_item_ring_one));
                imgvLine.setBackgroundResource(R.color.color_adapter_item_ring_one);
            } else if (position % 3 == 1) {
                txtCount.setBackgroundResource(R.color.color_adapter_item_ring_two);
                txtTitle.setTextColor(mContext.getResources().getColor(R.color.color_adapter_item_ring_two));
                imgvLine.setBackgroundResource(R.color.color_adapter_item_ring_two);
            } else {
                txtCount.setBackgroundResource(R.color.color_adapter_item_ring_three);
                txtTitle.setTextColor(mContext.getResources().getColor(R.color.color_adapter_item_ring_three));
                imgvLine.setBackgroundResource(R.color.color_adapter_item_ring_three);
            }
        }
    }
}
