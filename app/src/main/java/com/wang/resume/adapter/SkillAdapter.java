package com.wang.resume.adapter;

import android.content.Context;
import android.support.v4.util.ArrayMap;
import android.support.v7.widget.CardView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.Legend;
import com.wang.resume.R;
import java.util.ArrayList;
import java.util.List;
import cc.sdkutil.controller.inject.CCInjectUtil;
import cc.sdkutil.model.inject.CCInjectRes;

/**
 * Created by wangcong on 15-5-7.
 */
public class SkillAdapter extends BaseAdapter {

    public final static String PERCENT = "percent";       // 熟悉程度百分比  如： 85f
    public final static String TITLE = "title";
    public final static String CONTENT = "content";

    private Context mContext;
    private List<ArrayMap<String, String>> mAllList;

    public SkillAdapter(Context context, List<ArrayMap<String, String>> list) {
        this.mContext = context;
        this.mAllList = list;
    }

    @Override
    public int getCount() {
        return mAllList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Object getItem(int position) {
        return mAllList.get(position);
    }

    @Override
    public boolean isEnabled(int position) {
        return false;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        ArrayMap<String, String> map = mAllList.get(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.adapter_skill, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
            holder.update(map, position, true);
        } else {
            holder = (ViewHolder) convertView.getTag();
            holder.update(map, position, false);
        }
        return convertView;
    }

    static class ViewHolder {
        @CCInjectRes(R.id.skill_card)
        CardView cardView;
        @CCInjectRes(R.id.skill_piechart)
        PieChart mChart;
        @CCInjectRes(R.id.skill_count_indicator)
        TextView txtCount;
        @CCInjectRes(R.id.skill_title)
        TextView txtTitle;
        @CCInjectRes(R.id.skill_line)
        ImageView imgvLine;
        @CCInjectRes(R.id.skill_content)
        TextView txtContent;

        private Context mContext;

        public ViewHolder(View itemView) {
            mContext = itemView.getContext();
            CCInjectUtil.inject(this, itemView);
        }

        public void update(ArrayMap<String, String> map, int position, boolean animation) {
            if (animation) {
                AnimationSet set = (AnimationSet) AnimationUtils.loadAnimation(mContext,
                        position % 2 == 0 ? R.anim.anim_list_item_leftin : R.anim.anim_list_item_rightin);
                cardView.setAnimation(set);
            }
            txtCount.setText("0" + (position + 1));
            txtTitle.setText(map.get(TITLE));
            txtContent.setText(Html.fromHtml(map.get(CONTENT)));
            if (position % 3 == 0) {
                initPie(mContext.getResources().getColor(R.color.color_adapter_item_ring_one), map.get(PERCENT));
                txtCount.setBackgroundResource(R.color.color_adapter_item_ring_one);
                txtTitle.setTextColor(mContext.getResources().getColor(R.color.color_adapter_item_ring_one));
                imgvLine.setBackgroundResource(R.color.color_adapter_item_ring_one);
            } else if (position % 3 == 1) {
                initPie(mContext.getResources().getColor(R.color.color_adapter_item_ring_two), map.get(PERCENT));
                txtCount.setBackgroundResource(R.color.color_adapter_item_ring_two);
                txtTitle.setTextColor(mContext.getResources().getColor(R.color.color_adapter_item_ring_two));
                imgvLine.setBackgroundResource(R.color.color_adapter_item_ring_two);
            } else {
                initPie(mContext.getResources().getColor(R.color.color_adapter_item_ring_three), map.get(PERCENT));
                txtCount.setBackgroundResource(R.color.color_adapter_item_ring_three);
                txtTitle.setTextColor(mContext.getResources().getColor(R.color.color_adapter_item_ring_three));
                imgvLine.setBackgroundResource(R.color.color_adapter_item_ring_three);
            }
        }

        // 初始化饼图
        private void initPie(int color, String percent) {
            mChart.setHoleColor(mContext.getResources().getColor(R.color.color_window_bg));
            mChart.setDescription("");
            mChart.setHoleRadius(90);
            mChart.setDrawYValues(false);
            mChart.setDrawCenterText(true);
            mChart.setTouchEnabled(false);

            mChart.setDrawHoleEnabled(true);
            mChart.setDrawLegend(false);

            mChart.setRotationAngle(0);
            // 在饼状图上画出X的值
            mChart.setDrawXValues(false);
            // 可以根据手势进行旋转
            mChart.setRotationEnabled(false);
            // 饼状图中心
            mChart.setUsePercentValues(false);

            mChart.setCenterText(percent + "%");
            initPieData(color, Float.valueOf(percent));

            // 瓶装图的动画
            mChart.animateXY(1500, 1500);
            Legend l = mChart.getLegend();
            l.setPosition(Legend.LegendPosition.BELOW_CHART_CENTER);
            l.setXEntrySpace(7f);
            l.setYEntrySpace(5f);
        }

        /**
         * 设置饼图数据
         */
        private void initPieData(final int color, final float percent) {

            // 随机给饼状图添加数据
            ArrayList<Entry> tmpData = new ArrayList<Entry>() {
                {
                    add(new Entry(percent, 0));
                    add(new Entry(100 - percent, 0));
                }
            };

            ArrayList<String> xVals = new ArrayList<String>() {
                {
                    add("");
                    add("");
                }
            };

            PieDataSet set1 = new PieDataSet(tmpData, "");
            set1.setSliceSpace(3f);
            // 添加颜色
            ArrayList<Integer> colors = new ArrayList<Integer>() {
                {
                    add(color);
                    add(mContext.getResources().getColor(R.color.color_adapter_item_text_detail));
                }
            };

            colors.add(ColorTemplate.getHoloBlue());
            set1.setColors(colors);
            PieData data = new PieData(xVals, set1);
            mChart.setData(data);
            mChart.highlightValues(null);
            mChart.getLegend().setTextColor(color);//设置xy标签的值的颜色
            mChart.invalidate();
        }
    }

}
