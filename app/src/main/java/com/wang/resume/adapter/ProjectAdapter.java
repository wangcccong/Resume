package com.wang.resume.adapter;

import android.content.Context;
import android.support.v4.util.ArrayMap;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wang.resume.R;

import java.util.List;
import cc.sdkutil.controller.inject.CCInjectUtil;
import cc.sdkutil.model.inject.CCInjectRes;

/**
 * Created by wangcong on 15-5-5.
 */
public class ProjectAdapter extends RecyclerView.Adapter<ProjectAdapter.CusViewHolder> {

    public final static String TIME_START = "time_start";
    public final static String TIME_END = "time_end";
    public final static String PROJECT_NAME = "project_name";
    public final static String PROJECT_DETAIL = "project_detail";

    private Context mContext;
    private List<ArrayMap<String, String>> mAllList;

    public ProjectAdapter(Context context, List<ArrayMap<String, String>> list) {
        this.mContext = context;
        this.mAllList = list;
    }

    @Override
    public void onBindViewHolder(CusViewHolder holder, int position) {
        ArrayMap<String, String> map = mAllList.get(position);
        holder.update(map, position);
    }

    @Override
    public CusViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View convertView = LayoutInflater.from(mContext).inflate(R.layout.adapter_projectexp, parent, false);
        CusViewHolder holder = new CusViewHolder(convertView);
        return holder;
    }

    @Override
    public int getItemCount() {
        return mAllList.size();
    }

    static class CusViewHolder extends RecyclerView.ViewHolder {

        @CCInjectRes(R.id.project_linear)
        LinearLayout linear;
        @CCInjectRes(R.id.project_line)
        ImageView imgvLine;
        @CCInjectRes(R.id.project_count_indicator)
        TextView txtCount;
        @CCInjectRes(R.id.project_time_start)
        TextView txtTimeStart;
        @CCInjectRes(R.id.project_time_end)
        TextView txtTimeEnd;
        @CCInjectRes(R.id.project_name)
        TextView txtName;
        @CCInjectRes(R.id.project_detail)
        TextView txtDetail;

        private Context mContext;

        public CusViewHolder(View itemView) {
            super(itemView);
            this.mContext = itemView.getContext();
            CCInjectUtil.inject(this, itemView);
        }

        public void update(ArrayMap<String, String> map, int position) {
//            AnimationSet set = (AnimationSet) AnimationUtils.loadAnimation(mContext,
//                    position % 2 == 0 ? R.anim.anim_list_item_topin : R.anim.anim_list_item_bottomin);
//            linear.setAnimation(set);
            txtCount.setText("0" + (position + 1));
            if (position % 3 == 0) {
                txtCount.setBackgroundResource(R.drawable.shape_projectexp_ring_one);
                imgvLine.setBackgroundResource(R.color.color_adapter_item_ring_one);
                txtName.setTextColor(mContext.getResources().getColor(R.color.color_adapter_item_ring_one));
                txtTimeEnd.setTextColor(mContext.getResources().getColor(R.color.color_adapter_item_ring_one));
                txtTimeStart.setTextColor(mContext.getResources().getColor(R.color.color_adapter_item_ring_one));
            } else if (position % 3 == 1) {
                txtCount.setBackgroundResource(R.drawable.shape_projectexp_ring_two);
                imgvLine.setBackgroundResource(R.color.color_adapter_item_ring_two);
                txtName.setTextColor(mContext.getResources().getColor(R.color.color_adapter_item_ring_two));
                txtTimeEnd.setTextColor(mContext.getResources().getColor(R.color.color_adapter_item_ring_two));
                txtTimeStart.setTextColor(mContext.getResources().getColor(R.color.color_adapter_item_ring_two));
            } else {
                txtCount.setBackgroundResource(R.drawable.shape_projectexp_ring_three);
                imgvLine.setBackgroundResource(R.color.color_adapter_item_ring_three);
                txtName.setTextColor(mContext.getResources().getColor(R.color.color_adapter_item_ring_three));
                txtTimeEnd.setTextColor(mContext.getResources().getColor(R.color.color_adapter_item_ring_three));
                txtTimeStart.setTextColor(mContext.getResources().getColor(R.color.color_adapter_item_ring_three));
            }
            txtTimeStart.setText(map.get(TIME_START));
            txtTimeEnd.setText(map.get(TIME_END));
            txtName.setText(map.get(PROJECT_NAME));
            txtDetail.setText(Html.fromHtml(map.get(PROJECT_DETAIL)));
        }
    }
}
