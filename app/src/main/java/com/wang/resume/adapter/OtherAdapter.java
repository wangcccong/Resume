package com.wang.resume.adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.wang.resume.R;
import com.wang.resume.fragment.OtherFragment;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by wangcong on 15-5-7.
 */
public class OtherAdapter extends FragmentStatePagerAdapter {

    private final static String[] titles = {
            "UI加载",
            "图片处理",
            "数据缓存",
            "HTTP请求",
            "其他",
    };

    private List<Map<String, String>> mAllList;
    public OtherAdapter(FragmentManager fm, List<Map<String, String>> list) {
        super(fm);
        this.mAllList = list;
    }

    @Override
    public Fragment getItem(int position) {
        HashMap<String, String> map = (HashMap<String, String>) mAllList.get(position);
        map.put(OtherFragment.COLOR_BG, position % 2 == 0 ? R.color.color_other_item_one + ""
                : R.color.color_other_item_two + "");
        int color_element = R.color.color_adapter_item_ring_one;
        if (position % 3 == 1) {
            color_element = R.color.color_adapter_item_ring_two;
        } else if (position % 3 == 2) {
            color_element = R.color.color_adapter_item_ring_three;
        }
        map.put(OtherFragment.POSITION, position < 10 ? "0" + (position + 1) : (position + 1) + "");
        map.put(OtherFragment.COLOR_ELEMENT, color_element + "");
        OtherFragment fragment = new OtherFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("data", map);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public int getCount() {
        return titles.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titles[position];
    }
}
