package com.wang.resume.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.util.ArrayMap;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.Legend;
import com.wang.resume.R;
import com.wang.resume.adapter.ProjectAdapter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import cc.sdkutil.controller.util.CCToolUtil;
import cc.sdkutil.model.inject.CCInjectRes;
import cc.sdkutil.view.CCBaseActivity;

/**
 * Created by wangcong on 15-5-5.
 */
public class ProjectActivity extends CCBaseActivity {

    @CCInjectRes(R.id.project_toolbar)
    Toolbar mToolbar;
    @CCInjectRes(R.id.project_recyclerview)
    RecyclerView mRecyclerView;
    @CCInjectRes(R.id.project_piechart_one)
    PieChart mChartOne;
    @CCInjectRes(R.id.project_piechart_two)
    PieChart mChartTwo;

    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        setContentView(R.layout.activity_projectexp);

        initView();
    }

    private void initView() {

        mToolbar.setTitle("项目经验");
        setSupportActionBar(mToolbar);
        mToolbar.setNavigationIcon(R.mipmap.ic_nav_back);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        LinearLayoutManager manager = new LinearLayoutManager(this);
        manager.setOrientation(LinearLayoutManager.HORIZONTAL);
        mRecyclerView.setLayoutManager(manager);
        ProjectAdapter tmpAdapter = new ProjectAdapter(this, initData());
        mRecyclerView.setAdapter(tmpAdapter);

        initPieOne();
        initPieTwo();
    }

    // 初始化饼图
    private void initPieOne() {

        mChartOne.setDescription("");
        mChartOne.setHoleRadius(60f);
        mChartOne.setDrawYValues(true);
        mChartOne.setDrawCenterText(true);
        mChartOne.setTouchEnabled(false);

        mChartOne.setDrawHoleEnabled(true);

        mChartOne.setRotationAngle(0);
        // 在饼状图上画出X的值
        mChartOne.setDrawXValues(true);
        // 可以根据手势进行旋转
        mChartOne.setRotationEnabled(false);
        // 饼状图中心
        mChartOne.setUsePercentValues(true);

        mChartOne.setCenterText("公司/个人比例");
        initPieOneData();

        // 瓶装图的动画
        mChartOne.animateXY(1500, 1500);
        Legend l = mChartOne.getLegend();
        l.setPosition(Legend.LegendPosition.BELOW_CHART_CENTER);
        l.setXEntrySpace(7f);
        l.setYEntrySpace(5f);
    }

    /**
     * 设置饼图数据
     */
    private void initPieOneData() {

        // 随机给饼状图添加数据
        ArrayList<Entry> tmpData = new ArrayList<Entry>() {
            {
                add(new Entry(5.0f, 0));
                add(new Entry(1.0f, 0));
            }
        };

        ArrayList<String> xVals = new ArrayList<String>() {
            {
                add("公司");
                add("个人");
            }
        };

        PieDataSet set1 = new PieDataSet(tmpData, "");
        set1.setSliceSpace(3f);
        // 添加颜色
        ArrayList<Integer> colors = new ArrayList<Integer>() {
            {
                add(getResources().getColor(R.color.color_one));
                add(getResources().getColor(R.color.color_two));
            }
        };

        colors.add(ColorTemplate.getHoloBlue());
        set1.setColors(colors);
        PieData data = new PieData(xVals, set1);
        mChartOne.setData(data);
        mChartOne.highlightValues(null);
        mChartOne.getLegend().setTextColor(Color.BLACK);//设置xy标签的值的颜色
        mChartOne.invalidate();
    }

    // 初始化饼图
    private void initPieTwo() {

        mChartTwo.setDescription("");
        mChartTwo.setHoleRadius(60f);
        mChartTwo.setDrawYValues(true);
        mChartTwo.setDrawCenterText(true);
        mChartTwo.setTouchEnabled(false);

        mChartTwo.setDrawHoleEnabled(true);

        mChartTwo.setRotationAngle(0);
        // 在饼状图上画出X的值
        mChartTwo.setDrawXValues(true);
        // 可以根据手势进行旋转
        mChartTwo.setRotationEnabled(false);
        // 饼状图中心
        mChartTwo.setUsePercentValues(true);

        mChartTwo.setCenterText("项目平台比例");
        initPieTwoData();

        // 瓶装图的动画
        mChartTwo.animateXY(1500, 1500);
        Legend l = mChartTwo.getLegend();
        l.setPosition(Legend.LegendPosition.BELOW_CHART_CENTER);
        l.setXEntrySpace(7f);
        l.setYEntrySpace(5f);
    }

    /**
     * 设置饼图数据
     */
    private void initPieTwoData() {

        // 随机给饼状图添加数据
        ArrayList<Entry> tmpData = new ArrayList<Entry>() {
            {
                add(new Entry(3.0f, 0));
                add(new Entry(3.0f, 0));
            }
        };

        ArrayList<String> xVals = new ArrayList<String>() {
            {
                add("Android");
                add("iOS");
            }
        };

        PieDataSet set1 = new PieDataSet(tmpData, "");
        set1.setSliceSpace(3f);
        // 添加颜色
        ArrayList<Integer> colors = new ArrayList<Integer>() {
            {
                add(getResources().getColor(R.color.color_three));
                add(getResources().getColor(R.color.color_four));
            }
        };

        colors.add(ColorTemplate.getHoloBlue());
        set1.setColors(colors);
        PieData data = new PieData(xVals, set1);
        mChartTwo.setData(data);
        mChartTwo.highlightValues(null);
        mChartTwo.getLegend().setTextColor(Color.BLACK);//设置xy标签的值的颜色
        mChartTwo.invalidate();
    }

    /**
     * 初始化相关数据
     * @return
     */
    private List<ArrayMap<String, String>> initData() {
        return new ArrayList<ArrayMap<String, String>>() {
            {
                add(new ArrayMap<String, String>() {
                    {
                        put(ProjectAdapter.TIME_START, "2015 / 06");
                        put(ProjectAdapter.TIME_END, CCToolUtil.formatDate(new Date(), "yyyy / MM"));
                        put(ProjectAdapter.PROJECT_NAME, "动缘健身(体育运动)");
                        put(ProjectAdapter.PROJECT_DETAIL, "<font color=\"#493c46\">使用平台：</font><br>iOS<br><br>"
                                + "<font color=\"#493c46\">项目职责：</font><br>1、负责动缘健身iOS客户端动态、训练及我相关功能模块开发;<br>"
                                + "2、负责完善iOS自定义控件、HTTP请求框架<br><br>"
                                + "<font color=\"#493c46\">项目描述：</font><br>1、以体测为核心、健身房为基础的一款运动App<br>"
                                + "2、提供专业的国民体质评测：在合作场馆进行免费专业的国民体质评测及体脂检测，通过雷达图显示体测用户的身体状况及根据国民体测数据提供专业的建议;<br>"
                                + "3、量身定制的训练计划：根据上述国民体质评测结果通过定制的课程体系从四个方面（热身、训练、拉伸、整理）推荐课程<br>");
                    }
                });

                add(new ArrayMap<String, String>() {
                    {
                        put(ProjectAdapter.TIME_START, "2015 / 05 / 03");
                        put(ProjectAdapter.TIME_END, "2015 / 05 / 07");
                        put(ProjectAdapter.PROJECT_NAME, "个人简历");
                        put(ProjectAdapter.PROJECT_DETAIL, "<font color=\"#493c46\">使用平台：</font><br>Android<br><br>"
                                + "<font color=\"#493c46\">项目职责：</font><br>1、负责开发所有功能模块；<br>"
                                + "2、负责App项目UI设计<br><br>"
                                + "<font color=\"#493c46\">项目描述：</font><br>用于私人简历在手机端展现，方便携带，实现个人基本信息、基本技能、工作经验等");
                    }
                });

                add(new ArrayMap<String, String>() {
                    {
                        put(ProjectAdapter.TIME_START, "2014 / 11");
                        put(ProjectAdapter.TIME_END, "2015 / 06");
                        put(ProjectAdapter.PROJECT_NAME, "一刻达（O2O外面平台--消费者端）");
                        put(ProjectAdapter.PROJECT_DETAIL, "<font color=\"#493c46\">使用平台：</font><br>iOS<br><br>"
                                + "<font color=\"#493c46\">项目职责：</font><br>1、负责开发消费者端所有功能模块；<br>"
                                + "2、负责App项目框架代码重构（采用MVC模式重构以前所写框架）；<br>"
                                + "3、负责部分UI控件的自定义<br><br>"
                                + "<font color=\"#493c46\">项目描述：</font><br>消费者端是基于叫货模式的O2O平台，以便利店为切入点，"
                                + "实现语音、图片、文字叫货，商家抢单后由消费者选择是否让其服务，进而继续整个购物流程");
                    }
                });
                
                add(new ArrayMap<String, String>() {
                    {
                        put(ProjectAdapter.TIME_START, "2014 / 11");
                        put(ProjectAdapter.TIME_END, "2015 / 06");
                        put(ProjectAdapter.PROJECT_NAME, "一刻达（O2O外面平台--商家端）");
                        put(ProjectAdapter.PROJECT_DETAIL, "<font color=\"#493c46\">使用平台：</font><br>Android<br><br>"
                                + "<font color=\"#493c46\">项目职责：</font><br>1、负责开发商家端所有功能模块；<br>"
                                + "2、负责App项目框架代码编写（如：Http请求、部分自定义控件、图片加载处理）<br>"
                                + "3、负责部分UI控件的自定义<br><br>"
                                + "<font color=\"#493c46\">项目描述：</font><br>商家端是基于叫货模式的O2O平台，以便利店为切入点，"
                                + "抢单后等待顾客响应，继而完成整个交易流程");
                    }
                });
                
                add(new ArrayMap<String, String>() {
                    {
                        put(ProjectAdapter.TIME_START, "2013 / 10");
                        put(ProjectAdapter.TIME_END, "2014 / 11");
                        put(ProjectAdapter.PROJECT_NAME, "消防物联网智能终端");
                        put(ProjectAdapter.PROJECT_DETAIL, "<font color=\"#493c46\">使用平台：</font><br>iOS<br><br>"
                                + "<font color=\"#493c46\">项目职责：</font><br>1、负责开发指定功能模块（如：推送，在线测试，片区管理）；<br>"
                                + "2、负责App项目框架代码编写（如：Http请求、部分自定义控件）<br><br>"
                                + "<font color=\"#493c46\">项目描述：</font><br>消防物联网的智能终端基于iOS系统。根据整个消防国家标准，制定的一套消防物联网系统，"
                                + "主要包括各个角色权限，推送, 二维码的扫描，图片的上传，视频监控等");
                    }
                });
                
                add(new ArrayMap<String, String>() {
                    {
                        put(ProjectAdapter.TIME_START, "2013 / 06");
                        put(ProjectAdapter.TIME_END, "2014 / 11");
                        put(ProjectAdapter.PROJECT_NAME, "消防物联网智能终端");
                        put(ProjectAdapter.PROJECT_DETAIL, "<font color=\"#493c46\">使用平台：</font><br>Android<br><br>"
                                + "<font color=\"#493c46\">项目职责：</font><br>1、负责开发指定功能模块（如：远程监控，推送，在线测试）；<br>"
                                + "2、负责App项目框架代码编写（如：Http请求、缓存、UI初始化）<br><br>"
                                + "<font color=\"#493c46\">项目描述：</font><br>该系统是基于物联网开发的一套消防物联网系统，该系统包含远程监控，" +
                                "视频监控，维护管理，智能服务，消防考试系统等");
                    }
                });

            }
        };
    }
}
