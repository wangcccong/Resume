package com.wang.resume.activity;

import android.os.Bundle;
import android.support.v4.util.ArrayMap;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ListView;
import com.wang.resume.R;
import com.wang.resume.adapter.BasicInfoAdapter;
import java.util.ArrayList;
import java.util.List;
import cc.sdkutil.model.inject.CCInjectRes;
import cc.sdkutil.view.CCBaseActivity;

/**
 * Created by wangcong on 15-5-4.
 */
public class BasicInfoActivity extends CCBaseActivity {

    @CCInjectRes(R.id.basic_toolbar)
    Toolbar mToolbar;
    @CCInjectRes(R.id.basic_listview)
    ListView mListView;

    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        setContentView(R.layout.activity_basicinfo);

        initView();
    }

    private void initView() {
        mToolbar.setTitle("基本信息");
        setSupportActionBar(mToolbar);
        mToolbar.setNavigationIcon(R.mipmap.ic_nav_back);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        BasicInfoAdapter tmpAdapter = new BasicInfoAdapter(this, initData());
        mListView.setAdapter(tmpAdapter);
    }

    private List<ArrayMap<String, String>> initData() {
        return new ArrayList<ArrayMap<String, String>>() {
            {

                add(new ArrayMap<String, String>() {
                    {
                        put(BasicInfoAdapter.TITLE, "个人基本信息");
                        put(BasicInfoAdapter.CONTENT, "<font color=\"#493c46\">王聪</font><br>"
                                + "男 | 未婚 | 1991 年2月生 | 3年工作经验<br>"
                                + "户口：四川遂宁 | 现居住于四川成都<br>"
                                + "电话：15308220151<br>"
                                + "E-mail: wangcccong@outlook.com   1096497792@qq.com");
                    }
                });

                add(new ArrayMap<String, String>() {
                    {
                        put(BasicInfoAdapter.TITLE, "求职意向");
                        put(BasicInfoAdapter.CONTENT, "<font color=\"#493c46\">工作性质：</font>全职<br>"
                                + "<font color=\"#493c46\">期望职业：</font>软件工程师（iOS/Android移动应用开发）<br>"
                                + "<font color=\"#493c46\">期望行业：</font>互联网、计算机软件<br>"
                                + "<font color=\"#493c46\">工作地区：</font>成都<br>");
                    }
                });

                add(new ArrayMap<String, String>() {
                    {
                        put(BasicInfoAdapter.TITLE, "个人简介");
                        put(BasicInfoAdapter.CONTENT, "<font color=\"#493c46\">简介：</font>我就是一个码农，热爱代码，关注移动，喜欢iOS，偏爱Android，略有代码洁癖；" +
                                                                "休闲娱乐活动不多，至今不会唱一首完整歌曲，空闲之余爱看视频，记录片较好<br>" +
                                                                "关注军事如有闲暇可与小伙伴聊上一二；各种球类可以玩上一会儿，偏爱羽毛如遇对手连续半天何妨；<br><br>"
                                + "<font color=\"#493c46\">自我评价：</font>本人性格开朗、稳重、有活力，待人热情、真诚；工作认真负责，积极主动，能吃苦耐劳，勇于承受压力，勇于创新；对待工作认真负责，善于技术方面沟通。<br>");
                    }
                });

                add(new ArrayMap<String, String>() {
                    {
                        put(BasicInfoAdapter.TITLE, "教育经历");
                        put(BasicInfoAdapter.CONTENT, "<font color=\"#493c46\">时间：</font>2009/09 -- 2013/07<br>"
                                + "<font color=\"#493c46\">毕业院校：</font>成都信息工程学院 | 本科<br>"
                                + "<font color=\"#493c46\">所学专业：</font>网络工程<br>");
                    }
                });

                add(new ArrayMap<String, String>() {
                    {
                        put(BasicInfoAdapter.TITLE, "语言能力");
                        put(BasicInfoAdapter.CONTENT, "<font color=\"#493c46\">时间：</font>2010/06<br>"
                                + "<font color=\"#493c46\">语言等级：</font>英语四级<br>"
                                + "<font color=\"#493c46\">能力：</font>读写能力较好 | 听说能力一般<br>");
                    }
                });
            }
        };
    }
}
