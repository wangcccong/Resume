package com.wang.resume.activity;

import android.os.Bundle;
import android.support.v4.util.ArrayMap;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ListView;
import com.wang.resume.R;
import com.wang.resume.adapter.SkillAdapter;
import java.util.ArrayList;
import java.util.List;
import cc.sdkutil.model.inject.CCInjectRes;
import cc.sdkutil.view.CCBaseActivity;

/**
 * Created by wangcong on 15-5-4.
 */
public class SkillActivity extends CCBaseActivity {

    @CCInjectRes(R.id.skill_toolbar)
    Toolbar mToolbar;
    @CCInjectRes(R.id.skill_listview)
    ListView mListView;

    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        setContentView(R.layout.activity_skill);

        initView();
    }

    private void initView() {
        mToolbar.setTitle("基本技能");
        setSupportActionBar(mToolbar);
        mToolbar.setNavigationIcon(R.mipmap.ic_nav_back);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        SkillAdapter tmpAdapter = new SkillAdapter(this, initData());
        mListView.setAdapter(tmpAdapter);
    }

    private List<ArrayMap<String, String>> initData() {
        return new ArrayList<ArrayMap<String, String>>() {
            {

                add(new ArrayMap<String, String>() {
                    {
                        put(SkillAdapter.PERCENT, "80");
                        put(SkillAdapter.TITLE, "码农技能之IOS");
                        put(SkillAdapter.CONTENT, "熟悉Objective-C, swift开发; 能熟练使用iOS SDK中的UI控件、网络请求、数据库、XML/JSON解析等开发技巧；<br>" +
                                                "熟悉常用AFNetworking SDWebImage Masonry ReactiveCocoa响应式编程等框架<br> 有自定义UI、HTTP网络请求及缓存框架编写经验");
                    }
                });

                add(new ArrayMap<String, String>() {
                    {
                        put(SkillAdapter.PERCENT, "80");
                        put(SkillAdapter.TITLE, "码农技能之Android");
                        put(SkillAdapter.CONTENT, "<font color=\"#493c46\">熟悉：</font><br>1、熟悉Android四大组件；<br>"
                                + "2、熟悉常用UI控件，自定义UI无问题；<br>"
                                + "3、熟悉XML解析，JSON数据，图片压缩加载操作；<br>"
                                + "4、熟悉缓存处理，有缓存处理框架开发经验，及判断缓存中内容是否被系统回收；<br>"
                                + "<font color=\"#493c46\">了解：</font><br>能够使用Android NDK开发；");
                    }
                });

                add(new ArrayMap<String, String>() {
                    {
                        put(SkillAdapter.PERCENT, "80");
                        put(SkillAdapter.TITLE, "码农技能之JAVA");
                        put(SkillAdapter.CONTENT, "<font color=\"#493c46\">熟悉：</font><br>1、熟悉java基础，常用集合 lang（注解，反射，引用等常用）包 Util包常看；<br>"
                                + "2、熟悉IO操作；<br>"
                                + "3、熟悉java并发编程，代码之中基本不会出现使用显示线程操作；<br>"
                                + "4、熟悉HTTP，TCP/IP协议，有客户端http请求处理框架经验<br>"
                                + "<font color=\"#493c46\">了解：</font><br>了解JNI相关底层接口开发；");
                    }
                });

                add(new ArrayMap<String, String>() {
                    {
                        put(SkillAdapter.PERCENT, "40");
                        put(SkillAdapter.TITLE, "码农技能之设计模式");
                        put(SkillAdapter.CONTENT, "23中设计模式了解，熟练使用观察者、外观、建造者、单例、责任链、原型等模式");
                    }
                });
            }
        };
    }

}
