package com.wang.resume.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.util.ArrayMap;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ListView;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.Legend;
import com.wang.resume.R;
import com.wang.resume.adapter.JobAdapter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import cc.sdkutil.controller.util.CCToolUtil;
import cc.sdkutil.model.inject.CCInjectRes;
import cc.sdkutil.view.CCBaseActivity;

/**
 * Created by wangcong on 15-5-4.
 */
public class JobActivity extends CCBaseActivity {

    @CCInjectRes(R.id.job_toolbar)
    Toolbar mToolbar;
    @CCInjectRes(R.id.job_piechart)
    PieChart mChart;
    @CCInjectRes(R.id.job_listview)
    ListView mListView;

    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        setContentView(R.layout.activity_jobexp);

        initView();
    }

    private void initView() {
        mToolbar.setTitle("工作经验");
        setSupportActionBar(mToolbar);
        mToolbar.setNavigationIcon(R.mipmap.ic_nav_back);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        initPie();

        JobAdapter tmpAdapter = new JobAdapter(this, initData());
        mListView.setAdapter(tmpAdapter);
    }

    // 初始化饼图
    private void initPie() {

        mChart.setDescription("");
        mChart.setHoleRadius(60f);
        mChart.setDrawYValues(true);
        mChart.setDrawCenterText(true);
        mChart.setTouchEnabled(false);

        mChart.setDrawHoleEnabled(true);

        mChart.setRotationAngle(0);
        // 在饼状图上画出X的值
        mChart.setDrawXValues(true);
        // 可以根据手势进行旋转
        mChart.setRotationEnabled(false);
        // 饼状图中心
        mChart.setUsePercentValues(true);

        mChart.setCenterText("工作时长比例");
        initPieData();

        // 瓶装图的动画
        mChart.animateXY(1500, 1500);
        Legend l = mChart.getLegend();
        l.setPosition(Legend.LegendPosition.BELOW_CHART_CENTER);
        l.setXEntrySpace(7f);
        l.setYEntrySpace(5f);
    }

    /**
     * 设置饼图数据
     */
    private void initPieData() {

        // 随机给饼状图添加数据
        ArrayList<Entry> tmpData = new ArrayList<Entry>() {
            {
                add(new Entry((float) ((float)(System.currentTimeMillis()
                        -  CCToolUtil.parseDate("2015-06-02 00:00:00", "yyyy-MM-dd HH:mm:ss").getTime())), 0));
                add(new Entry((float) ((float)(CCToolUtil.parseDate("2015-06-02 00:00:00", "yyyy-MM-dd HH:mm:ss").getTime()
                   -  CCToolUtil.parseDate("2014-11-17 00:00:00", "yyyy-MM-dd HH:mm:ss").getTime())), 0));
                add(new Entry((float) ((float)(CCToolUtil.parseDate("2014-11-17 00:00:00", "yyyy-MM-dd HH:mm:ss").getTime()
                        -  CCToolUtil.parseDate("2013-06-20 00:00:00", "yyyy-MM-dd HH:mm:ss").getTime())), 0));
            }
        };

        ArrayList<String> xVals = new ArrayList<String>() {
            {
                add("成都四方");
                add("成都朗锐芯");
                add("成都安吉斯");
            }
        };

        PieDataSet set1 = new PieDataSet(tmpData, "");
        set1.setSliceSpace(3f);
        // 添加颜色
        ArrayList<Integer> colors = new ArrayList<Integer>() {
            {
                add(getResources().getColor(R.color.color_five_nor));
                add(getResources().getColor(R.color.color_four_nor));
                add(getResources().getColor(R.color.color_two_nor));
            }
        };

        colors.add(ColorTemplate.getHoloBlue());
        set1.setColors(colors);
        PieData data = new PieData(xVals, set1);
        mChart.setData(data);
        mChart.highlightValues(null);
        mChart.getLegend().setTextColor(Color.BLACK);//设置xy标签的值的颜色
        mChart.invalidate();
    }

    /**
     * 初始化相关数据
     * @return
     */
    private List<ArrayMap<String, String>> initData() {
        return new ArrayList<ArrayMap<String, String>>() {
            {
                add(new ArrayMap<String, String>() {
                    {
                        put(JobAdapter.TIME_START, "2015 / 06");
                        put(JobAdapter.TIME_END, CCToolUtil.formatDate(new Date(), "yyyy / MM"));
                        put(JobAdapter.COMPANY_NAME, "成都四方信息技术有限公司");
                        put(JobAdapter.JOB_DETAIL, "<font color=\"#493c46\">工作岗位：</font><br>iOS移动终端研发工程师。<br><br>"
                                + "<font color=\"#493c46\">工作描述：</font> <br>负责iOS手机终端软件开发工作。<br><br>"
                                + "<font color=\"#493c46\">主要职责：</font><br>1、负责公司项目客户开发及文档编写工作;<br>"
                                + "2、负责将Android框架重构移植到iOS <br>"
                                + "3、负责自定义控件、软件需求开发及后期bug修改工作。<br>");
                    }
                });

                add(new ArrayMap<String, String>() {
                    {
                        put(JobAdapter.TIME_START, "2014 / 11");
                        put(JobAdapter.TIME_END, "2015 / 06");
                        put(JobAdapter.COMPANY_NAME, "成都朗锐芯科技发展有限公司");
                        put(JobAdapter.JOB_DETAIL, "<font color=\"#493c46\">工作岗位：</font><br>移动终端研发工程师。<br><br>"
                                + "<font color=\"#493c46\">工作描述：</font> <br>主要负责手机终端（Android 及 IOS）软件开发工作。<br><br>"
                                + "<font color=\"#493c46\">主要职责：</font><br>1、负责公司项目客户开发及文档编写工作;<br>"
                                + "2、负责Android框架重构（采用MVC），涉及Http、缓存、图片处理; <br>"
                                + "3、负责IOS框架编写（按照Android设计模式）。<br>");
                    }
                });

                add(new ArrayMap<String, String>() {
                    {
                        put(JobAdapter.TIME_START, "2013 / 06");
                        put(JobAdapter.TIME_END, "2014 / 11");
                        put(JobAdapter.COMPANY_NAME, "成都安吉斯信息科技有限公司");
                        put(JobAdapter.JOB_DETAIL, "<font color=\"#493c46\">工作岗位：</font><br>移动终端研发工程师。<br><br>"
                                + "<font color=\"#493c46\">工作描述：</font><br>主要负责公司手机终端（Android 及 IOS）软件的开发及服务器端接口。<br><br>"
                                + "<font color=\"#493c46\">主要职责：</font><br>1、负责指定模块代码与接口文档编写;<br>"
                                + "  2、完成指定的软件功能模块的开发和验证; <br> 3、解决后期bug。<br>");
                    }
                });
            }
        };
    }
}
