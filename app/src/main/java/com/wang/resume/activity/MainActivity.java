package com.wang.resume.activity;

import android.os.Bundle;
import android.support.v4.widget.SlidingPaneLayout;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import com.wang.resume.R;
import com.wang.resume.fragment.MainContentFragment;
import com.wang.resume.fragment.MainMenuFragment;
import cc.sdkutil.controller.util.CCToastUtil;
import cc.sdkutil.controller.view.CCAppManager;
import cc.sdkutil.model.inject.CCInjectRes;
import cc.sdkutil.view.CCBaseActivity;

public class MainActivity extends CCBaseActivity {

    @CCInjectRes(R.id.main_slidingpanel)
    SlidingPaneLayout mSlidingPanelLayout;

    @CCInjectRes(R.id.main_toolBar)
    Toolbar mToolBar;

    private MainMenuFragment menuFragment;
    private MainContentFragment contentFragment;

    // 记录按返回键的时间
    private long mBackRecordTime = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initView();
    }

    /**
     * 初始化view相关数据
     */
    private void initView() {

        menuFragment = new MainMenuFragment();
        contentFragment = new MainContentFragment();
        getSupportFragmentManager().beginTransaction()
                .add(R.id.main_slidingpanel_left, menuFragment)
                .add(R.id.main_slidingpanel_content, contentFragment)
                .commit();

        mToolBar.setTitle("个人简历");
        setSupportActionBar(mToolBar);
//        mToolBar.setNavigationIcon(R.mipmap.ic_action_user);
//        mToolBar.setNavigationOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (mSlidingPanelLayout.isOpen()) {
//                    mSlidingPanelLayout.closePane();
//                } else {
//                    mSlidingPanelLayout.openPane();
//                }
//            }
//        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_download:
                mSlidingPanelLayout.openPane();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            // 如果侧边栏打开，则先关闭侧边栏
            if (mSlidingPanelLayout.isOpen()) {
                mSlidingPanelLayout.closePane();
                return true;
            }

            // 连续按两次退出程序
            if (System.currentTimeMillis() - 1000 > mBackRecordTime) {
                mBackRecordTime = System.currentTimeMillis();
                CCToastUtil.showShort(MainActivity.this, "再按一次退出");
                return false;
            } else {
                CCAppManager.newInstance().finishApp(MainActivity.this);
                return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }
}
