package com.wang.resume.activity;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.nineoldandroids.view.ViewHelper;
import com.wang.resume.R;
import com.wang.resume.adapter.OtherAdapter;
import com.wang.resume.fragment.OtherFragment;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import cc.sdkutil.model.inject.CCInjectRes;
import cc.sdkutil.view.CCBaseActivity;
import cc.sdkutil.view.CCPagerSlidingTabStripCompat;
import cc.sdkutil.view.CCViewPagerCompat;

/**
 * Created by wangcong on 15-5-5.
 */
public class OtherActivity extends CCBaseActivity {

    @CCInjectRes(R.id.other_toolbar)
    Toolbar mToolbar;
    @CCInjectRes(R.id.other_tabstrip)
    CCPagerSlidingTabStripCompat mtabStrip;
    @CCInjectRes(R.id.other_viewpager)
    CCViewPagerCompat mViewPager;

    private static final float MIN_SCALE = 0.75f;

    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        setContentView(R.layout.activity_other);

        initView();
    }

    private void initView() {
        mToolbar.setTitle("自定义框架");
        setSupportActionBar(mToolbar);
        mToolbar.setNavigationIcon(R.mipmap.ic_nav_back);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        OtherAdapter tmpAdapter = new OtherAdapter(getSupportFragmentManager(), initData());
        mViewPager.setAdapter(tmpAdapter);
        mtabStrip.setViewPagerCompat(mViewPager);
        mViewPager.setPageTransformer(true, new CCViewPagerCompat.PageTransformer() {
            @Override
            public void transformPage(View view, float position) {
                int pageWidth = view.getWidth();

                if (position < -1) {
                    ViewHelper.setAlpha(view, 0);
                } else if (position <= 0) {// a页滑动至b页 ； a页从 0.0 -1 ；b页从1 ~ 0.0
                    ViewHelper.setAlpha(view, 1);
                    ViewHelper.setTranslationX(view, 0);
                    ViewHelper.setScaleX(view, 1);
                    ViewHelper.setScaleY(view, 1);

                } else if (position <= 1) {
                    ViewHelper.setAlpha(view, 1 - position);
                    ViewHelper.setTranslationX(view, pageWidth * -position);
                    float scaleFactor = MIN_SCALE + (1 - MIN_SCALE) * (1 - position);
                    ViewHelper.setScaleX(view, scaleFactor);
                    ViewHelper.setScaleY(view, scaleFactor);
                } else {
                    ViewHelper.setAlpha(view, 1);
                }
            }
        });
    }
    public final static String TITLE = "title";
    public final static String FUNC = "func";
    public final static String SKILL = "skill";
    private List<Map<String, String>> initData() {
        return new ArrayList<Map<String, String>>() {
            {
                add(new HashMap<String, String>() {
                    {
                        put(OtherFragment.TITLE, "UI加载模块");
                        put(OtherFragment.FUNC, "通过注解方式获取定义在项目文件中的相关数据，如控件、颜色、字符串、图片等资源,通过动态代理方式为View设置监听");
                        put(OtherFragment.SKILL, "Android 加载xml控件、Java注解、Java反射机制、Java模板类、常用集合、控制反转设计模式");
                    }
                });

                add(new HashMap<String, String>() {
                    {
                        put(OtherFragment.TITLE, "图片处理模块");
                        put(OtherFragment.FUNC, "1. 实现基于传入比例压缩及质量压缩：解决网络图片加载压缩问题<br>"
                                + "2. 图片加载：图片加载控制类，使用Builder创建一个图片加载对象，根据需要设置是否需要缓存到本地，设置是否缩放，" +
                                "设置最大加载数据数量（该功能对ListView加载图片时有帮助），增加自动取消过早加载的图片功能<br>");
                        put(OtherFragment.SKILL, "Android图片处理、HTTP协议、Java并发编程、Java IO/NIO、建造者及外观设计模式");
                    }
                });

                add(new HashMap<String, String>() {
                    {
                        put(OtherFragment.TITLE, "缓存数据模块");
                        put(OtherFragment.FUNC, "支持内存缓存（LRU）和磁盘缓存，可检测内存缓存对象是否被JVM回收，自动清除已回收对象并计算当前缓存大小，自定义 内存缓存大小，清空缓存");
                        put(OtherFragment.SKILL, "JDK7 异常处理、Java IO/NIO、Java引用、Java集合自带的LRU算法、单例设计模式");
                    }
                });

                add(new HashMap<String, String>() {
                    {
                        put(OtherFragment.TITLE, "HTTP请求模块");
                        put(OtherFragment.FUNC, "1、HTTP访问客户端支持同步与异步访问两种方式，初始化客户端发起http请求;<br>"
                                + "2、支持编码方式、cookie、userAgent（支持设备识别，版本识别）等设置；<br>"
                                +"3、支持（POST/GET/PUT/HEAD/DELETE）方式访问服务器端，默认支持gzip， deflate压缩；<br>"
                                + "4、支持文字、文件、输入流、字节多种形式及多文件上传，支持断点续传功能；支持上传或下载查看进度；支持取消操作<br>");
                        put(OtherFragment.SKILL, "HTTP协议、Java 网络处理、Java集合、Java IO/NIO");
                    }
                });

                add(new HashMap<String, String>() {
                    {
                        put(OtherFragment.TITLE, "其他功能");
                        put(OtherFragment.FUNC, "1、框架异步处理消息设计，可用于任何需要发送异步消息的工作，涉及立即发送异步消息、延时发送、延时循环发送，方便扩展并重写异步处理逻辑及处理成功响应反馈逻辑；<br>"
                                + "2、重定义的观察者模式等其他功能；<br>"
                                + "3、调用一个方法parse解析多层Json数据，调用一个enclose方法将对象封装为Json字符串（解决字符中带有特殊字符的问题）<br>");
                        put(OtherFragment.SKILL, "1、Json解析方法、正则表达式、递归算法；<br>"
                                + "2、Android 消息处理机制 Looper、Message、MessageQueue、Handler、HandlerThread<br>");
                    }
                });
            }
        };
    }
}
